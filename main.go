package main

import (
	"fmt"
	"time"

	"gitlab.com/gokultp/gitlab-status/request"
	"gitlab.com/gokultp/gitlab-status/timer"
)

func main() {
	var respTime, connTime, dataTime time.Duration
	count := 0
	stop := timer.ExecAtEvery(time.Second*10, func() {
		r, c, d, err := request.GetResponseTime("https://gitlab.com")
		if err == nil {
			respTime += r
			connTime += c
			dataTime += d
			count += 1
			fmt.Println("|\t", r, "|\t", c, "|\t", d, "|")
		}
	})

	time.Sleep(time.Minute * 5)
	stop <- true
	fmt.Println("-----------------------------------------------------------")
	fmt.Println("No of requests:", count)
	fmt.Println("Avg Response time: ", time.Duration(int64(respTime)/int64(count)))
	fmt.Println("Avg Connection time: ", time.Duration(int64(connTime)/int64(count)))
	fmt.Println("Avg Datatransfer time: ", time.Duration(int64(dataTime)/int64(count)))

}
