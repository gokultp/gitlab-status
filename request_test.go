package main_test

import (
	"testing"

	"gitlab.com/gokultp/gitlab-status/request"
)

// Testing GetResponseTime to check if there is any errors in response
func TestGetResponseTimeError(t *testing.T) {
	var ResponseTimeErrorCheckCases = []struct {
		url    string
		isNull bool
	}{{url: "https://gitlab.com", isNull: true},
		{url: "htt://gitlab.com", isNull: false},
	}

	for _, tCase := range ResponseTimeErrorCheckCases {
		_, _, _, err := request.GetResponseTime(tCase.url)

		if err == nil && !tCase.isNull {
			t.Error("Should throw error")
		} else if err != nil && tCase.isNull {
			t.Error("Should not throw error")
		}
	}
}

func TestGetResponseTime(t *testing.T) {
	var ResponseTimeErrorCheckCases = []struct {
		url string
	}{{url: "https://gitlab.com"},
		{url: "http://google.com"},
		{url: "http://github.com"},
		{url: "http://saasgrids.com"},
	}

	for _, tCase := range ResponseTimeErrorCheckCases {
		respTime, connTime, dataTime, err := request.GetResponseTime(tCase.url)

		if err != nil {
			t.Error("Should not throw error")
		} else if respTime == 0 {
			t.Error("Response time cannot be zero")
		} else if connTime == 0 {
			t.Error("Connection time cannot be zero")
		} else if dataTime == 0 {
			t.Error("Connection time cannot be zero")
		}

	}

}
