package request

import (
	"net"
	"net/http"
	"time"
)

// Transport is a custom transport for making requests
type Transport struct {
	roundTripper   http.RoundTripper
	dialer         *net.Dialer
	connStart      time.Time
	connEnd        time.Time
	roundTripStart time.Time
	roundTripEnd   time.Time
}

func GetResponseTime(url string) (responseTime time.Duration, connTime time.Duration, dataTime time.Duration, err error) {
	transport := newTransport()
	client := &http.Client{Transport: transport}
	_, err = client.Get(url)
	return transport.ResponseTime(), transport.ConnTime(), transport.DataTransferTime(), err
}

func newTransport() *Transport {

	r := &Transport{
		dialer: &net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		},
	}
	r.roundTripper = &http.Transport{
		Proxy:               http.ProxyFromEnvironment,
		Dial:                r.dial,
		TLSHandshakeTimeout: 10 * time.Second,
	}
	return r
}

func (r *Transport) RoundTrip(req *http.Request) (*http.Response, error) {
	r.roundTripStart = time.Now()
	resp, err := r.roundTripper.RoundTrip(req)
	r.roundTripEnd = time.Now()
	return resp, err
}

func (r *Transport) dial(network, addr string) (net.Conn, error) {
	r.connStart = time.Now()
	cn, err := r.dialer.Dial(network, addr)
	r.connEnd = time.Now()
	return cn, err
}

func (r *Transport) DataTransferTime() time.Duration {
	return r.ResponseTime() - r.ConnTime()
}

func (r *Transport) ConnTime() time.Duration {
	return r.connEnd.Sub(r.connStart)
}

func (r *Transport) ResponseTime() time.Duration {
	return r.roundTripEnd.Sub(r.roundTripStart)
}
