package timer

import (
	"time"
)

func ExecAtEvery(duration time.Duration, exec func()) chan bool {
	ticker := time.NewTicker(duration)
	stop := make(chan bool, 1)

	go func() {
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				exec()
			case <-stop:
				return
			}
		}
	}()

	return stop
}
